///<reference path="../../node_modules/@angular/http/src/http_module.d.ts"/>
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { FormsModule } from '@angular/forms';

import {routing} from './app.routes';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import {QuizService} from "./_services/quiz.service";
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    QuizComponent,
    ModalComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule,
      routing
  ],
  providers: [QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }
