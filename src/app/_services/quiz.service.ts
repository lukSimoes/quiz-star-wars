import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";


@Injectable()
export class QuizService {


    ACCESS_KEY = '&safe_search=1&per_page=&page=1&format=json&nojsoncallback=1';
    API_URL_IMG = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=6e17e88675ab1648c70345c091d475a1';
    constructor(private httpClient: HttpClient) {}
    getAll(api_url) {
       return this.httpClient.get(api_url);
    }

    getImage(name) {
        return this.httpClient.get(this.API_URL_IMG + '&text==' + name + this.ACCESS_KEY);
    }


}
