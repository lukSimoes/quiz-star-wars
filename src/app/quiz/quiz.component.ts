import { Component, OnInit, AfterViewInit, ViewChild  } from '@angular/core';
import {QuizService} from "../_services/quiz.service";
import {ModalComponent} from "../modal/modal.component";


@Component({
    selector: 'app-quiz',
    templateUrl: './quiz.component.html',
    styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit, AfterViewInit {
    minutesToShow: any = '02';
    secondsToShow: any = '00';
    people: any = [];
    nextPage: any ;
    score: any = 0;
    topRank: any = [];


    @ViewChild(ModalComponent)  modal: ModalComponent;
    constructor(private _service: QuizService) {
    }
    ngAfterViewInit() {
        this.countDown();
    }
    ngOnInit() {
        this.getPeople('https://swapi.co/api/people/');

    }
    countDown() {
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;
        let  minutes, seconds, now, distance,
            calcminutes, calcseconds;
        const currentHour = new Date();
        const countDown = new Date(currentHour.getTime() + 2.02 * 60000).getTime();
        const x = setInterval(() => {
            now = new Date().getTime();
            distance = countDown - now;
            calcminutes = Math.floor((distance % (hour)) / (minute));
            calcminutes > 0 ? minutes = calcminutes : minutes = '00';

            calcseconds = Math.floor((distance % (minute)) / second);
            calcseconds > 0 ? seconds = calcseconds : seconds = '00';
            minutes < 10 && minutes !== '00' ? minutes = '0' + minutes : minutes = minutes;
            seconds < 10 && seconds !== '00' ? seconds = '0' + seconds : seconds = seconds;
            this.minutesToShow = minutes;
            this.secondsToShow = seconds;

            if (calcminutes <= 0 && calcseconds <= 0) {
                clearInterval(x);
                localStorage.setItem('currentScore', this.score);
              this.modal.open('#modaFormulario', false);


            }
        }, second);

    }
    getPeople(url) {
        this._service.getAll(url).subscribe((data: any) => {

            if ('next' in data) {
                this.nextPage = data['next'];
            }
            data['results'].forEach((item, index) => {
                this._service.getImage(item.name).subscribe((image: any) => {
                    let photo: any = [];
                    image.photos.photo.length ? photo = image.photos.photo[0] : photo = [];
                   const url_img = "https://farm" + photo.farm + ".staticflickr.com/" + photo.server +
                       "/" + photo.id + "_" + photo.secret + "_n.jpg";

                        data['results'][index]['images'] = url_img;


                });
            });

            this.people = this.people.concat(data['results']);

        });
    }



    openModal(person) {
        const dicas = ['homeworld', 'films', 'starships', 'species', 'vehicles'];
        person.help = true;
        dicas.forEach((e, index) => {
            let urlRequest =  '';
            if (!Array.isArray(person[e])) {
                urlRequest = person[e];
                this._service.getAll(urlRequest).subscribe( data => {
                    person[e] = data;
                });
            } else {
                for (let i = 0; i <= person[e].length - 1; i++) {
                    urlRequest = person[e][i];
                    this._service.getAll(urlRequest).subscribe( data => {
                        person[e][i] = data;
                    });
                }
                this.modal.open('#modal01', person);

            }
        });

    }

    checkAnswer(i) {
        if ((this.people[i].answer.toLowerCase() === this.people[i].name.toLowerCase()) && !this.people[i].scored) {
            this.people[i].scored = true;
            if (this.people[i].help) {
                this.score = this.score + 5;
            } else {
                this.score = this.score + 10;
            }
        }
    }

    saveScore (event) {
        event.score = this.score;
        this.topRank.push(event);
        localStorage.setItem('rank', JSON.stringify(this.topRank));
        this.modal.close();
        this.modal.alert('#alert', 'pontuação salva!');
        document.documentElement.classList.remove('is-scroll-disabled');

    }


}
