import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {HomeComponent} from "./home/home.component";
import {QuizComponent} from "./quiz/quiz.component";
const  ROUTES: Routes = [

    {path: '', component: HomeComponent, } ,
    {path: 'quiz', component: QuizComponent, } ,
];

export const routing: ModuleWithProviders = RouterModule.forRoot(ROUTES);
