import { Component, OnInit, ViewChild } from '@angular/core';
import {ModalComponent} from "../modal/modal.component";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   valueRank: any = JSON.parse(localStorage.getItem('rank'));
    @ViewChild(ModalComponent)  modal: ModalComponent;
  constructor() { }

  ngOnInit() {
  }

  showRank() {

    this.valueRank = JSON.parse(localStorage.getItem('rank'));
     const order = this.valueRank.sort(function(a, b) {
          return a.score > b.score ? -1 : a.score < b.score ? 1 : 0;
      });
     this.modal.listRank = this.valueRank;
      this.modal.open('#modalRank');



  }

}
