import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router} from '@angular/router';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
    openedModal: any;
    person: any = ['homeworld', 'films', 'starships'];
    emailUser: any ;
    nameUser: any ;
    currentScore: any;
    listRank = [];
    @Output() saveTheScore = new EventEmitter();


    constructor( private _router: Router) {
        this.person['homewolrd'] = [];
        this.person['films'] = [];
        this.person['starships'] = [];
        this.person['species'] = [];
        this.person['vehicles'] = [];
    }

    ngOnInit() {
    }

    open (target, contentModal? , callback?) {
        this.currentScore = localStorage.getItem('currentScore');
        if (contentModal) {
            this.person = contentModal;
        }
        document.documentElement.classList.add('is-scroll-disabled');
        this.openedModal = document.querySelector(target);
        this.openedModal.dataset.opened = 'true';
        callback && callback(this.openedModal);

        // Add youtube iframe video src or video src
        if (this.openedModal.dataset.video) {
            if (this.openedModal.dataset.video.includes('youtube') ) {
                const vParam = this.openedModal.dataset.video.split('v=');
                const videoId = vParam[1].includes('&') ? vParam[1].substr(0, vParam[1].indexOf('&')) : vParam[1];
                const embed = 'https://www.youtube.com/embed/' + videoId + '?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1';
                this.openedModal.querySelector('iframe').src = embed;
            } else {
                this.openedModal.querySelector('video').src = this.openedModal.dataset.video;
            }
        }
        // Add iframe src
        if (this.openedModal.dataset.iframe) {
            this.openedModal.querySelector('iframe').src = this.openedModal.dataset.iframe;
        }
    }

    close (event?, callback?) {
        if (event) {
            event.preventDefault(), event.stopPropagation();
            if (!event.target.dataset.hasOwnProperty('modalClose')) { return false; }
        }

        document.documentElement.classList.remove('is-scroll-disabled');
        this.openedModal.dataset.opened = 'false';

        // Remove video iframe url or iframe src
        if ( this.openedModal.dataset.iframe ) {
            this.openedModal.querySelector('iframe').src = '';
        }

        // Remove video iframe src or video src
        if ( this.openedModal.dataset.video ) {
            if ( this.openedModal.dataset.video.includes('youtube') ) {
                this.openedModal.querySelector('iframe').src = '';
            } else {
                this.openedModal.querySelector('video').pause();
            }
        }

        callback && callback(this.openedModal);
    }
    change (target, event?) {
        event && (event.preventDefault(), event.stopPropagation());
        this.openedModal.dataset.opened = 'false';
        this.openedModal = document.querySelector(target);
        this.openedModal.dataset.opened = 'true';
    }

    alert (element, text) {
        this.open(element, false, function(element) {
            text && (element.querySelector('.modal--content').innerHTML = text);
        });
    }

    saveScore() {
        this.saveTheScore.emit({'nameUser': this.nameUser, 'emailUser': this.emailUser});

    }

    goHome () {
        this._router.navigate(['/']);
    }

}
